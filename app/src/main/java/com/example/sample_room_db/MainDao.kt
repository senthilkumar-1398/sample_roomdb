package com.example.sample_room_db

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.Query

@Dao
interface MainDao {

    @Insert
    fun insert(mainData: MainData)

    @Delete
    fun delete(mainData: MainData)

    @Delete
    fun reset(mainData: List<MainData>)

    @Query("Select * from userdata")
    fun getAll(): List<MainData>

    @Query("UPDATE userdata SET name= :sName,email=:sEmail WHERE ID = :sID")
    fun update(sID: Int, sName: String, sEmail: String)

}
