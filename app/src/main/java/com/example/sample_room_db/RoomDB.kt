package com.example.sample_room_db

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase

@Database(entities = [MainData::class], version = 1, exportSchema = false)
abstract class RoomDB : RoomDatabase() {
    abstract fun mainDao(): MainDao

    companion object {
        private var mRoomDB: RoomDB? = null
        @JvmStatic
        fun getInstance(context: Context?): RoomDB {

            // When calling this instance concurrently from multiple threads we're in serious trouble:
            if (mRoomDB == null) {
                mRoomDB = Room.databaseBuilder(context!!.applicationContext, RoomDB::class.java, "user_database").allowMainThreadQueries().fallbackToDestructiveMigration()
                        .build()
            }
            return mRoomDB!!
        }
    }

}