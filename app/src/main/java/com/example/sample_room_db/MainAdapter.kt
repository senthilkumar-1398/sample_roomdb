package com.example.sample_room_db

import android.app.Dialog
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.widget.*
import androidx.recyclerview.widget.RecyclerView

class MainAdapter(private var mDataList: ArrayList<MainData>, private val mContext: Context) :
    RecyclerView.Adapter<MainAdapter.CustomViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CustomViewHolder {
        var view: View = LayoutInflater.from(parent.context)
            .inflate(R.layout.recycle_item, parent, false)
        return CustomViewHolder(view)
    }

    override fun getItemCount(): Int {
        return mDataList.size
    }

    override fun onBindViewHolder(holder: CustomViewHolder, position: Int) {
        var mRoomDB = RoomDB.getInstance(mContext)
        var mDtaList = mDataList[position]

        holder.mName.text = mDtaList.name
        holder.mEmail.text = mDtaList.email

        holder.mEditButton.setOnClickListener {
            var editData: MainData = mDataList[holder.adapterPosition]
            var id: Int = editData.id!!
            var name: String = editData.name
            var email: String = editData.email

            var dialog = Dialog(mContext!!)
            dialog.setContentView(R.layout.update_layout)
            var width: Int = WindowManager.LayoutParams.MATCH_PARENT
            var height: Int = WindowManager.LayoutParams.WRAP_CONTENT
            dialog.window!!.setLayout(width, height)
            dialog.show()

            //Update Data

            var mEditName = dialog.findViewById<EditText>(R.id.edittext_updatename)
            var mEditEmail = dialog.findViewById<EditText>(R.id.edittext_emailupdate)
            var mUpdateButton = dialog.findViewById<Button>(R.id.btn_update)
            mEditName.setText(name)
            mEditEmail.setText(email)

            mUpdateButton.setOnClickListener {
                dialog.dismiss()
                var updateName: String = mEditName.text.toString().trim()
                var updateEmail: String = mEditEmail.text.toString().trim()
                mRoomDB!!.mainDao()!!.update(id, updateName, updateEmail)
                mDataList.clear()
                mDataList.addAll(mRoomDB.mainDao().getAll())
                notifyDataSetChanged()
                Toast.makeText(it.context, "Record Updated Successfully", Toast.LENGTH_SHORT).show()
            }
        }

        holder.mDeleteButton.setOnClickListener {
            var mDeleteData: MainData = mDataList.get(holder.adapterPosition)
            mRoomDB.mainDao().delete(mDeleteData)
            var position: Int = holder.adapterPosition
            mDataList.removeAt(position)
            notifyItemRemoved(position)
            notifyItemRangeChanged(position, mDataList.size)
            Toast.makeText(it.context, "Record Deleted Successfully", Toast.LENGTH_SHORT).show()
        }
    }

    class CustomViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var mName = itemView.findViewById<TextView>(R.id.textview_name2)!!
        var mEmail = itemView.findViewById<TextView>(R.id.textview_email2)!!
        var mEditButton = itemView.findViewById<ImageView>(R.id.imageview_edit2)!!
        var mDeleteButton = itemView.findViewById<ImageView>(R.id.imageview_delete2)!!

    }


}








