package com.example.sample_room_db

import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    private var mAdapter: MainAdapter? = null
    private var mDataList: ArrayList<MainData> = ArrayList()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        var mRoomDB = RoomDB.getInstance(this)
        mDataList = mRoomDB.mainDao().getAll() as ArrayList<MainData>
        var mLinearLayoutManager: LinearLayoutManager = LinearLayoutManager(this)
        rv_list.layoutManager = mLinearLayoutManager
        mAdapter = MainAdapter(mDataList, this)
        rv_list.adapter = mAdapter

        btn_add.setOnClickListener {
            var mName: String = edittext_name.text.toString().trim()
            var mEmail: String = edittext_email.text.toString().trim()

            if (mName.isNotEmpty() && mEmail.isNotEmpty()) {
                var mainData: MainData = MainData()
                mainData.name = mName
                mainData.email = mEmail
                mRoomDB!!.mainDao()!!.insert(mainData)
                edittext_name.text = null
                edittext_email.text = null
                mDataList.clear()
                mDataList.addAll(mRoomDB!!.mainDao()!!.getAll())
                mAdapter!!.notifyDataSetChanged()
                Toast.makeText(this, "Added Successfully", Toast.LENGTH_SHORT).show()
            } else {
                Toast.makeText(this, "Please Enter required Field", Toast.LENGTH_SHORT).show()
            }
        }

        btn_reset.setOnClickListener {
            mRoomDB.mainDao().reset(mDataList)
            mDataList.clear()
            mDataList.addAll(mRoomDB.mainDao().getAll())
            mAdapter!!.notifyDataSetChanged()
            Toast.makeText(this, "Reset Successfully", Toast.LENGTH_SHORT).show()
        }
    }

}